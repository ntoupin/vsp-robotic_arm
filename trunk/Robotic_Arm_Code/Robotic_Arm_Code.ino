/*
   @file Robotic_Arm_Code.ino
   @author Nicolas Toupin
   @date 2019-11-24
   @brief Robotic arm project.
*/

#include <NT_Pwm.h>
#include <NT_Servo.h>
#include <NT_Pid.h>

Pwm_t Pwm1;
Pwm_t Pwm2;
Pwm_t Pwm3;
Pwm_t Pwm4;
Pwm_t Pwm5;
Pwm_t Pwm6;
Servo_t Servo1;
Servo_t Servo2;
Servo_t Servo3;
Servo_t Servo4;
Servo_t Servo5;
Servo_t Servo6;
Pid_t Pid1;
Pid_t Pid2;
Pid_t Pid3;
Pid_t Pid4;
Pid_t Pid5;
Pid_t Pid6;

void setup()
{
  // Configure pwm, servo motor and pid for servo motor 1
  Servo1.Configure(0, 0, 0, 1000, 0, 256);
  Servo1.Init();
  Pwm1.Configure(0, 0, Servo1.Minimum_Op, Servo1.Maximum_Op, 0, 255);
  Pwm1.Attach(9); // Set pin of Pwm
  Pwm1.Init();
  Pid1.Configure(1, 0, 0);
  Pid1.Init();

  // Configure pwm, servo motor and pid for servo motor 2
  Servo2.Configure(0, 0, 0, 2000, 0, 256);
  Servo2.Init();
  Pwm2.Configure(0, 0, Servo2.Minimum_Op, Servo2.Maximum_Op, 0, 255);
  Pwm2.Attach(9); // Set pin of Pwm
  Pwm2.Init();
  Pid2.Configure(2, 0, 0);
  Pid2.Init();

  // Configure pwm, servo motor and pid for servo motor 3
  Servo3.Configure(0, 0, 0, 3000, 0, 256);
  Servo3.Init();
  Pwm3.Configure(0, 0, Servo3.Minimum_Op, Servo3.Maximum_Op, 0, 255);
  Pwm3.Attach(9); // Set pin of Pwm
  Pwm3.Init();
  Pid3.Configure(3, 0, 0);
  Pid3.Init();

  // Configure pwm, servo motor and pid for servo motor 4
  Servo4.Configure(0, 0, 0, 4000, 0, 256);
  Servo4.Init();
  Pwm4.Configure(0, 0, Servo4.Minimum_Op, Servo4.Maximum_Op, 0, 255);
  Pwm4.Attach(9); // Set pin of Pwm
  Pwm4.Init();
  Pid4.Configure(4, 0, 0);
  Pid4.Init();

  // Configure pwm, servo motor and pid for servo motor 5
  Servo5.Configure(0, 0, 0, 5000, 0, 256);
  Servo5.Init();
  Pwm5.Configure(0, 0, Servo5.Minimum_Op, Servo5.Maximum_Op, 0, 255);
  Pwm5.Attach(9); // Set pin of Pwm
  Pwm5.Init();
  Pid5.Configure(5, 0, 0);
  Pid5.Init();

  // Configure pwm, servo motor and pid for servo motor 6
  Servo6.Configure(0, 0, 0, 6000, 0, 256);
  Servo6.Init();
  Pwm6.Configure(0, 0, Servo6.Minimum_Op, Servo6.Maximum_Op, 0, 255);
  Pwm6.Attach(9); // Set pin of Pwm
  Pwm6.Init();
  Pid6.Configure(6, 0, 0);
  Pid6.Init();
}

void loop()
{
  float Servo1_Value_Sp = 10;
  Servo1.Set(Pid1.Compute(Servo1_Value_Sp, Pwm1.Value_Sp));
  Pwm1.Set(Servo1.Value_Op);

  float Servo2_Value_Sp = 10;
  Servo2.Set(Pid2.Compute(Servo2_Value_Sp, Pwm2.Value_Sp));
  Pwm2.Set(Servo2.Value_Op);

  float Servo3_Value_Sp = 30;
  Servo3.Set(Pid3.Compute(Servo3_Value_Sp, Pwm3.Value_Sp));
  Pwm3.Set(Servo3.Value_Op);

  float Servo4_Value_Sp = 40;
  Servo4.Set(Pid4.Compute(Servo4_Value_Sp, Pwm4.Value_Sp));
  Pwm4.Set(Servo4.Value_Op);

  float Servo5_Value_Sp = 50;
  Servo5.Set(Pid5.Compute(Servo5_Value_Sp, Pwm5.Value_Sp));
  Pwm5.Set(Servo5.Value_Op);

  float Servo6_Value_Sp = 60;
  Servo6.Set(Pid6.Compute(Servo6_Value_Sp, Pwm6.Value_Sp));
  Pwm6.Set(Servo6.Value_Op);
}
